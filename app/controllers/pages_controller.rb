class PagesController < ApplicationController
    require 'mechanize'
    require 'geoip'
    require 'addressable/uri'

    def index
        @pages = Page.all
        @geoip ||= GeoIP.new('GeoLiteCity.dat')
    end
 
    def new
        @page = Page.new
    end

def create 
    @page = Page.new(page_params)
    if verify_recaptcha(model: @page) && @page.save
        PageMailer.page_title(@page).deliver_now
        redirect_to '/pages'
    else
        render "new"
    end

end

    private 
    
    def page_params 
        params.require(:page).permit(:name, :email, :url)
    end

end