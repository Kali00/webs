class PageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.page_mailer.page_title.subject
  #

  def page_title(page)
    
    @page = page
    mail(to: page.email, subject: "Page title and location")
  end

end
