class Page < ApplicationRecord

    validates :name, presence: true
#validates email address
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false}
#validates URL
    validates :url, url: true
    validates :url, format: { with: URI.regexp }, allow_blank: true

    # after_create :send_address

    # private

    # def send_address
    #     PageMailer.page_title(self).deliver_now
    # end
end
