class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :name
      t.string :email
      t.string :url

      t.text :address
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
