# Preview all emails at http://localhost:3000/rails/mailers/page_mailer
class PageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/page_mailer/page_title
  def page_title
    page = Page.first
    PageMailer.page_title
  end

end
