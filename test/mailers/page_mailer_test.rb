require 'test_helper'

class PageMailerTest < ActionMailer::TestCase
  test "page_title" do
    mail = PageMailer.page_title
    assert_equal "Page title", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
