module Location
    require 'socket'
    require 'net/http'
    require 'json'
  
    def get_ip_address(url)
      IPSocket::getaddress(url)
    end
  
    def get_location_info(url)
      ip_address = get_ip_address(url)
      location = Net::HTTP.get(URI("https://ipapi.co/#{ip_address}/json/"))
      JSON.parse(location)
    end
  
    module_function :get_ip_address
    module_function :get_location_info
  end